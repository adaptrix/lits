@extends('layouts.app')

@section('content')

<div class="page_title">
<div class="container">

    <div class="title"><h1>Services</h1></div>
        
	<h3>Storytelling fused with technology and design.</h3>
	<h5>Anchour enhances business with its proven web media services.</h5>
    
</div>
</div><!-- end page title -->

<div class="clearfix"></div>

<div class="content_fullwidth less">

<div class="container">
    <div class="clearfix marb4"></div>

    <ul class="clogo_small">
        <li><h6 class="gray"><em>As see on:</em></h6></li>
        
        <li><img src="images/clientlogo11-gray.png" alt="" /></li>
        <li><img src="images/clientlogo12-gray.png" alt="" /></li>
        <li><img src="images/clientlogo13-gray.png" alt="" /></li>
        <li><img src="images/clientlogo14-gray.png" alt="" /></li>
        <li><img src="images/clientlogo15-gray.png" alt="" /></li>
        <li><img src="images/clientlogo16-gray.png" alt="" /></li>
        <li><img src="images/clientlogo17-gray.png" alt="" /></li>
        <li><img src="images/clientlogo18-gray.png" alt="" /></li>
        <li><img src="images/clientlogo19-gray.png" alt="" /></li>
    </ul>
    
    <div class="margin_top2"></div>
</div><!-- end section -->

<div class="clearfix"></div>

<div class="features_sec59 two">
<div class="container">
	
    <div class="one_full stcode_title9">
    
    	<h2>Featured Services
        <span class="line"></span></h2>

    </div>
    
    <div class="clearfix marb4"></div>
    
    <div id="owl-demo6" class="owl-carousel">
    
            <div>
            
            	<div class="one_half"><img src="{{asset('images/site-img84.jpg')}}" alt="" /></div>
                
                <div class="one_half last">
                
                	<h3 class="color">Several Design Options</h3>
                	
                    <h6 class="gray">Many desktop publishing packages and web page editors now use as their default model text, and a search.</h6>
                    
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
			</div><!-- end section -->
            
            <div>
            
            	<div class="one_half"><img src="{{asset('images/site-img85.jpg')}}" alt="" /></div>
                
                <div class="one_half last">
                
                	<h3 class="color">Clean &amp; Modern Design</h3>
                	
                    <h6 class="gray">Many desktop publishing packages and web page editors now use as their default model text, and a search.</h6>
                    
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
			</div><!-- end section -->
            
            <div>
            
            	<div class="one_half"><img src="{{asset('images/site-img86.jpg')}}" alt="" /></div>
                
                <div class="one_half last">
                
                	<h3 class="color">Build Your Owne Website</h3>
                	
                    <h6 class="gray">Many desktop publishing packages and web page editors now use as their default model text, and a search.</h6>
                    
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
			</div><!-- end section -->
               
              
		</div>

</div>
</div><!-- end features section 59 -->

<div class="clearfix"></div>

<div class="features_sec3">
<div class="container">

	<div class="title2">
    	<h2><span class="line"></span><span class="text">Why Choose Us</span><em>Aipsum therefore always</em></h2>
    </div>
    
    <div class="clearfix margin_top3"></div>
    
    <div class="one_third">
    
        <div class="box">
        
            <div class="ciref outline-outward left"> <span aria-hidden="true" class="icon-screen-desktop"></span> </div>
            
            <div class="right">
                <h5>Modern Design</h5>
                <p>Mombined with handful model sentence structures to generate which looks.</p>
            </div>
		
        </div><!-- end section -->
    	
        <div class="box">
        
            <div class="ciref outline-outward left"> <span aria-hidden="true" class="icon-umbrella"></span> </div>
            
            <div class="right">
                <h5>Flexibility</h5>
                <p>Mombined with handful model sentence structures to generate which looks.</p>
            </div>
		
        </div><!-- end section -->
        
        <div class="box last">
        
            <div class="ciref outline-outward left"> <span aria-hidden="true" class="icon-settings"></span> </div>
            
            <div class="right">
                <h5>Parallax Effects</h5>
                <p>Mombined with handful model sentence structures to generate which looks.</p>
            </div>
		
        </div><!-- end section -->
        
	</div><!-- end all sections -->
    
    <div class="one_third">
    
        <div class="box">
        
            <div class="ciref outline-outward left active"> <span aria-hidden="true" class="icon-social-dropbox"></span> </div>
            
            <div class="right">
                <h5>Diffrent Websites</h5>
                <p>Mombined with handful model sentence structures to generate which looks.</p>
            </div>
		
        </div><!-- end section -->
    	
        <div class="box">
        
            <div class="ciref outline-outward left"> <span aria-hidden="true" class="icon-social-youtube"></span> </div>
            
            <div class="right">
                <h5>Background Videos</h5>
                <p>Mombined with handful model sentence structures to generate which looks.</p>
            </div>
		
        </div><!-- end section -->
        
        <div class="box last">
        
            <div class="ciref outline-outward left"> <span aria-hidden="true" class="icon-bell"></span> </div>
            
            <div class="right">
                <h5>Contact Form</h5>
                <p>Mombined with handful model sentence structures to generate which looks.</p>
            </div>
		
        </div><!-- end section -->
        
	</div><!-- end all sections -->
    
    <div class="one_third last">
    
        <div class="box">
        
            <div class="ciref outline-outward left"> <span aria-hidden="true" class="icon-cup"></span> </div>
            
            <div class="right">
                <h5>Mega Menu</h5>
                <p>Mombined with handful model sentence structures to generate which looks.</p>
            </div>
		
        </div><!-- end section -->
    	
        <div class="box">
        
            <div class="ciref outline-outward left"> <span aria-hidden="true" class="icon-layers"></span> </div>
            
            <div class="right">
                <h5>Layered PSD Files</h5>
                <p>Mombined with handful model sentence structures to generate which looks.</p>
            </div>
		
        </div><!-- end section -->
        
        <div class="box last">
        
            <div class="ciref outline-outward left"> <span aria-hidden="true" class="icon-support"></span> </div>
            
            <div class="right">
                <h5>Cross Browser Support</h5>
                <p>Mombined with handful model sentence structures to generate which looks.</p>
            </div>
		
        </div><!-- end section -->
        
	</div><!-- end all sections -->

</div>
</div><!-- end features section 3 -->

<div class="clearfix"></div>

<div class="parallax_section1">
<div class="container">
	
    <h2>Great Value for Your Money to Get the Aaika Theme on ThemeForest Only.</h2>
    
    <p>Many desktop publishin packages and web page editors search versions have over the years sometimes on purpose.</p>
    
    <a href="#" class="button transp">Purchase Now!</a>

</div>
</div><!-- end parallax section 1 -->

<div class="clearfix"></div>

<div class="container">
	<div class="marb12"></div>

    <div class="one_full stcode_title6">
    	<h2>We are  <strong>Experts</strong>  On</h2>
        <br />
	</div>
    
	<div class="piechart1"><canvas class="loader"></canvas> <br /> UI/UX Design </div>
    <div class="piechart1"><canvas class="loader2"></canvas> <br /> Development </div>
    <div class="piechart1"><canvas class="loader3"></canvas> <br /> Javascript </div>
    <div class="piechart1"><canvas class="loader4"></canvas> <br /> Wordpress </div>
    <div class="piechart1"><canvas class="loader5"></canvas> <br /> Graphic Design </div>
    
    <script>
	(function($) {
 	"use strict";
	
	$(document).ready(function() {
		$('.loader').ClassyLoader({
			percentage: 90,
			speed: 30,
			fontSize: '50px',
			diameter: 90,
			lineColor: 'rgba(19,175,235,1)',
			remainingLineColor: 'rgba(200,200,200,0.4)',
			lineWidth: 9
		});
	});

	$(document).ready(function() {
		$('.loader2').ClassyLoader({
			percentage: 100,
			speed: 30,
			fontSize: '50px',
			diameter: 90,
			lineColor: 'rgba(63,195,95,1)',
			remainingLineColor: 'rgba(200,200,200,0.4)',
			lineWidth: 9
		});
	});
	
	$(document).ready(function() {
		$('.loader3').ClassyLoader({
			percentage: 85,
			speed: 30,
			fontSize: '50px',
			diameter: 90,
			lineColor: 'rgba(252,66,66,1)',
			remainingLineColor: 'rgba(200,200,200,0.4)',
			lineWidth: 9
		});
	});
	
	$(document).ready(function() {
		$('.loader4').ClassyLoader({
			percentage: 65,
			speed: 30,
			fontSize: '50px',
			diameter: 90,
			lineColor: 'rgba(47,146,238,1)',
			remainingLineColor: 'rgba(200,200,200,0.4)',
			lineWidth: 9
		});
	});

	$(document).ready(function() {
		$('.loader5').ClassyLoader({
			percentage: 95,
			speed: 30,
			fontSize: '50px',
			diameter: 90,
			lineColor: 'rgba(199,98,203,1)',
			remainingLineColor: 'rgba(200,200,200,0.4)',
			lineWidth: 9
		});
	});


	})(jQuery);
	</script><!-- end section -->

	<div class="margin_top12"></div>
</div>



</div><!-- end content area -->

@endsection