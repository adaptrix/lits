<header class="header">
 
	<div class="container">
    
    <!-- Logo -->
    <div class="logo"><a href="/" id="logo"></a></div>
		
	<!-- Navigation Menu -->
	<nav class="menu_main">
        
	<div class="navbar yamm navbar-default">
    
    <div class="container">
      <div class="navbar-header">
        <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  > <span>Menu</span>
          <button type="button" > <i class="fa fa-bars"></i></button>
        </div>
      </div>
      
      <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
      
        <ul class="nav navbar-nav">
        
			
			<li class="yamm-fw"><a href="{{ url('/') }}" class="{{Request::is('/')? 'active':''}}">Home</a></li>
			
			<li class="yamm-fw"><a href="{{ url('/about-us') }}" class="{{Request::is('about-us')? 'active':''}}">About Us</a></li>
				   
			<li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Services</a>
				<ul class="dropdown-menu multilevel" role="menu">
					<li><a href="{{ url('services/web-designing') }}" class="{{Request::is('services/web-designing')? 'active':''}}">Web Designing</a></li>
					<li><a href="#">Mobile Application</a></li>
					<li><a href="#">Software Development</a></li>
					<li><a href="#">Video Adverts</a></li>
					<li><a href="#">Digital Marketing</a></li>
					<li><a href="#">Graphic Designing</a></li>
					<li><a href="#">Branding</a></li>
					<li><a href="#">Presentation</a></li>
					
				</ul>
			</li>
			
			<li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Products</a>
				<ul class="dropdown-menu multilevel" role="menu">
					<li><a href="#">Dukafy</a></li>
					<li><a href="#">Automate</a></li>
					<li><a href="#">Transfer Me</a></li>
					<li><a href="#">Remind Me</a></li>
					<li><a href="#">Legendary Transportation</a></li>
					
				</ul>
			</li>
			
			
			<li class="yamm-fw"><a href="{{ url('/portfolio') }}" class="{{Request::is('portfolio')? 'active':''}}">Portfolio</a></li>
			
			<li class="yamm-fw"><a href="#">News & Events</a></li>
			
			<li class="yamm-fw"><a href="{{ url('/contact-us') }}" class="{{Request::is('contact-us')? 'active':''}}">Contact Us</a></li>
        </ul>
        
      </div>
      </div>
     </div>
     
	</nav><!-- end Navigation Menu -->
    
    <div class="menu_right"><a href="#" class="buynow">Offers</a></div>
    
	</div>
    
</header>