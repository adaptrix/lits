<div id="sliderWrap">

	<div id="openCloseIdentifier"></div>
    
        <div id="slider">
        
          	<div id="sliderContent">
            <div class="container">
          
            	<div class="one_fourth">
                
                	<h5 class="white">About Legendary IT Solutions</h5>
                    
                    We are proud to have the ability to look at problems and then offer a mixture of out of the box solutions combined with Bespoke (Custom) Software and web development to make a tailored solution that meets your requirements.
                
                </div>
                
                <div class="one_fourth">
                
                	<h5 class="white">Showcase Works</h5>
                    
                <div id="grid-container2" class="cbp-l-grid-fullScreen two">
                
                <ul>
                
                    <li class="cbp-item">
                        <a href="images/media/portfolio-img1.jpg" class="cbp-caption cbp-lightbox" data-title="Dashboard<br>by GSRthemes9">
                            <div class="cbp-caption-defaultWrap">
                                <img src="images/media/portfolio-img1.jpg" alt="">
                            </div>
                            <div class="cbp-caption-activeWrap"></div>
                        </a>
                    </li>
                    
                    
                    <li class="cbp-item">
                        <a href="images/media/portfolio-img2.jpg" class="cbp-caption cbp-lightbox" data-title="Client chat app WIP<br>by GSRthemes9">
                            <div class="cbp-caption-defaultWrap">
                                <img src="images/media/portfolio-img2.jpg" alt="">
                            </div>
                            <div class="cbp-caption-activeWrap"></div>
                        </a>
                    </li>
                    
                    
                    <li class="cbp-item">
                        <a href="images/media/portfolio-img3.jpg" class="cbp-caption cbp-lightbox" data-title="World Clock Widget<br>by GSRthemes9">
                            <div class="cbp-caption-defaultWrap">
                                <img src="images/media/portfolio-img3.jpg" alt="">
                            </div>
                            <div class="cbp-caption-activeWrap"></div>
                        </a>
                    </li>
                    
                    
                    <li class="cbp-item">
                        <a href="images/media/portfolio-img4.jpg" class="cbp-caption cbp-lightbox" data-title="Website Lightbox<br>by GSRthemes9">
                            <div class="cbp-caption-defaultWrap">
                                <img src="images/media/portfolio-img4.jpg" alt="">
                            </div>
                            <div class="cbp-caption-activeWrap"></div>
                        </a>
                    </li>
                    
                    
                    <li class="cbp-item">
                        <a href="images/media/portfolio-img5.jpg" class="cbp-caption cbp-lightbox" data-title="Skateshop Website<br>by GSRthemes9">
                            <div class="cbp-caption-defaultWrap">
                                <img src="images/media/portfolio-img5.jpg" alt="">
                            </div>
                            <div class="cbp-caption-activeWrap"></div>
                        </a>
                    </li>
                    
                    
                    <li class="cbp-item">
                        <a href="images/media/portfolio-img6.jpg" class="cbp-caption cbp-lightbox" data-title="10 Navigation Bars<br>by GSRthemes9">
                            <div class="cbp-caption-defaultWrap">
                                <img src="images/media/portfolio-img6.jpg" alt="">
                            </div>
                            <div class="cbp-caption-activeWrap"></div>
                        </a>
                    </li>
                    
                    
                    <li class="cbp-item">
                        <a href="images/media/portfolio-img7.jpg" class="cbp-caption cbp-lightbox" data-title="To-Do Dashboard<br>by GSRthemes9">
                            <div class="cbp-caption-defaultWrap">
                                <img src="images/media/portfolio-img7.jpg" alt="">
                            </div>
                            <div class="cbp-caption-activeWrap"></div>
                        </a>
                    </li>
                    
                    
                    <li class="cbp-item">
                        <a href="images/media/portfolio-img8.jpg" class="cbp-caption cbp-lightbox" data-title="Events and  More<br>by GSRthemes9">
                            <div class="cbp-caption-defaultWrap">
                                <img src="images/media/portfolio-img8.jpg" alt="">
                            </div>
                            <div class="cbp-caption-activeWrap"></div>
                        </a>
                    </li>
                	
                    <li class="cbp-item">
                        <a href="images/media/portfolio-img8.jpg" class="cbp-caption cbp-lightbox" data-title="Events and  More<br>by GSRthemes9">
                            <div class="cbp-caption-defaultWrap">
                                <img src="images/media/portfolio-img8.jpg" alt="">
                            </div>
                            <div class="cbp-caption-activeWrap"></div>
                        </a>
                    </li>
                     
                </ul>
                
                </div>
                    
                </div>
                
                <div class="one_fourth">
                
                	<h5 class="white">Instagram Feeds</h5>
                	
                    <ul class="list_5">
                    
                        <li><a href="index1.html"><img src="images/t-thumb1.jpg" alt="" /> <em>Style One</em></a></li>
                        <li><a href="index2.html"><img src="images/t-thumb2.jpg" alt="" /> <em>Style Two</em></a></li>
                        <li><a href="index4.html"><img src="images/t-thumb3.jpg" alt="" /> <em>Style Three</em></a></li>
                        <li class="last"><a href="index5.html"><img src="images/t-thumb4.jpg" alt="" /> <em>Style Four</em></a></li>
                    
                    </ul>
                
                </div>
                
                <div class="one_fourth last">
                
                	<h5 class="white">Get in Touch</h5>
                    
                    <ul class="faddress">
                        
                        <li><i class="fa fa-map-marker fa-lg"></i>&nbsp; Mtendeni Street, Opposite Mtedeni 
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;School, Next to Takkims Holiday <br> 
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tanzania, Dar es Salaam, P.O. Box 2632</li>
                        <li><i class="fa fa-phone"></i>&nbsp; +255 754 025098 / +255 717 344 295</li>
                        <li><i class="fa fa-envelope"></i>&nbsp; <a href="mailto:info@legendaryits.com">info@legendaryits.com</a></li>
                        <li><i class="fa fa-globe"></i>&nbsp; <a href="http://legendaryits.com">www.legendaryits.com</a></li>
                        <li><i class="fa fa-clock-o"></i>&nbsp; Mon - Fri 9:00am - 5:00 pm</li>
                    </ul>
                    
				</div>
                
            </div>    
            </div>
            
        </div>
    
	<div id="openCloseWrap"><a href="#" class="topMenuAction" id="topMenuImage"><img src="js/slidepanel/open.png" alt="open" title="open" /></a></div>
        
</div><!-- end slide wrap -->