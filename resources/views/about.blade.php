@extends('layouts.app')

@section('content')

<div class="page_title">
<div class="container">

    <div class="title"><h1>About Us</h1></div>
        
	<h3>Storytelling fused with technology and design.</h3>
	<h5>Anchour enhances business with its proven web media services.</h5>
    
</div>
</div><!-- end page title -->

<div class="clearfix"></div>

<div class="content_fullwidth less">


<div class="clearfix"></div>

<div class="features_sec60" style="background: #f4f4f4;">
<div class="container">
	<div class="onecol_thirtyfive last">
    
		<div class="stcode_title4">
            
        	<h3><span class="line"></span><span class="text">More About<span></span></span></h3>
                
    	</div><!-- end section -->
        
        <img src="images/site-img87.jpg" alt="" class="img_left2" />
            
		<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking aits layout The point of using lorem is that it has a more less normal distribution of letters opposed to using content here content. <br /><br /> Many desktop publishing packages web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.</p>
        
    </div><!-- end all section -->
    <div class="onecol_thirtyfive" style="margin-right: 30px !important;">
    
		<div class="stcode_title4">
            
        	<h3><span class="line"></span><span class="text">What We Offer<span></span></span></h3>
                
    	</div><!-- end section -->
        
        <div class="lirts">
        
            <span aria-hidden="true" class="icon-screen-desktop"></span> <h5>Many desktop packages</h5>
            
            <p>Many desktop publishing packages and web page editors now use lorem psum as their default mode text and a search for will cover many web sites.</p>
        
        </div><!-- end section -->
        
		<div class="clearfix margin_top3"></div>

        <div class="lirts">
        
            <span aria-hidden="true" class="icon-cup"></span> <h5>Have evolved over years</h5>
            
            <p>Many desktop publishing packages and web page editors now use lorem psum as their default mode text and a search for will cover many web sites.</p>
        
        </div><!-- end section -->
        
        <div class="clearfix margin_top3"></div>
        
        <div class="lirts">
        
            <span aria-hidden="true" class="icon-support"></span> <h5>Embarrassing in the middle</h5>
            
            <p>Many desktop publishing packages and web page editors now use lorem psum as their default mode text and a search for will cover many web sites.</p>
        
        </div><!-- end section -->
        
	</div><!-- end all section -->
    
    <div class="one_fourth">
    
    	<div class="stcode_title4">
            
        	<h3><span class="line"></span><span class="text">Why Us<span></span></span></h3>
                
    	</div><!-- end section -->
    	
        <p>Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their sometimes on purpose readable oans apage when looking at its layouts.</p>
        
        <br />
        
        <ul class="list1 small">
        	
            <li><a href="#"><i class="fa fa-chevron-right"></i> 14 Different Homepages</a></li>
            <li><a href="#"><i class="fa fa-chevron-right"></i> Web Design &amp; Development</a></li>
            <li><a href="#"><i class="fa fa-chevron-right"></i> E-Commerce Development</a></li>
            <li><a href="#"><i class="fa fa-chevron-right"></i> Mobile/Responsive Development</a></li>
            <li><a href="#"><i class="fa fa-chevron-right"></i> Facebook Designed Layouts</a></li>
            <li><a href="#"><i class="fa fa-chevron-right"></i> Experimental Design &amp; Technology</a></li>

        </ul>
        
    </div>
    
	
    
    
</div>
</div><!-- end features section 60 -->


<div class="content_fullwidth">

<div class="container">
	
	<div class="stcode_title4">
            
        	<h3><span class="line"></span><span class="text">Services<span></span></span></h3>
                
    	</div>
	<div class="one_fourth ">
    
        <div class="flips3 active">
        
            <div class="flips3_front flipscont3">
            	<i class="fa fa-paper-plane-o"></i>
            	<h5>Web Designing</h5>
            </div>
            
            <div class="flips3_back flipscont3">
            	<h5 class="white">Web Designing</h5>
                <p>Lorem Ipsum as the default will uncover many web sites versions have years.</p>
                <br />
                <a href="#" class="but_small5 light2"><i class="fa fa-paper-plane"></i>&nbsp; Read more</a>
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fourth">
    
        <div class="flips3">
        
            <div class="flips3_front flipscont3">
            	<i class="fa fa-check-square-o"></i>
            	<h5>Mobile Application</h5>
            </div>
            
            <div class="flips3_back flipscont3">
            	<h5 class="white">Mobile Application</h5>
                <p>Lorem Ipsum as the default will uncover many web sites versions have years.</p>
                <br />
                <a href="#" class="but_small5 light2"><i class="fa fa-paper-plane"></i>&nbsp; Read more</a>
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fourth">
    
        <div class="flips3 ">
        
            <div class="flips3_front flipscont3">
            	<i class="fa fa-building-o"></i>
            	<h5>Software Development</h5>
            </div>
            
            <div class="flips3_back flipscont3">
            	<h5 class="white">Software Development</h5>
                <p>Lorem Ipsum as the default will uncover many web sites versions have years.</p>
                <br />
                <a href="#" class="but_small5 light2"><i class="fa fa-paper-plane"></i>&nbsp; Read more</a>
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fourth last">
    
        <div class="flips3">
        
            <div class="flips3_front flipscont3">
            	<i class="fa fa-smile-o"></i>
            	<h5>Video Adverts</h5>
            </div>
            
            <div class="flips3_back flipscont3">
            	<h5 class="white">Video Adverts</h5>
                <p>Lorem Ipsum as the default will uncover many web sites versions have years.</p>
                <br />
                <a href="#" class="but_small5 light2"><i class="fa fa-paper-plane"></i>&nbsp; Read more</a>
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="clearfix"></div>
    <div class="margin_top3"></div>
    
    	<div class="one_fourth">
    
        <div class="flips3">
        
            <div class="flips3_front flipscont3">
            	<i class="fa fa-laptop"></i>
            	<h5>Digital Marketing</h5>
            </div>
            
            <div class="flips3_back flipscont3">
            	<h5 class="white">Digital Marketing</h5>
                <p>Lorem Ipsum as the default will uncover many web sites versions have years.</p>
                <br />
                <a href="#" class="but_small5 light2"><i class="fa fa-paper-plane"></i>&nbsp; Read more</a>
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fourth">
    
        <div class="flips3">
        
            <div class="flips3_front flipscont3">
            	<i class="fa fa-comment-o"></i>
            	<h5>Graphic Designing</h5>
            </div>
            
            <div class="flips3_back flipscont3">
            	<h5 class="white">Graphic Designing</h5>
                <p>Lorem Ipsum as the default will uncover many web sites versions have years.</p>
                <br />
                <a href="#" class="but_small5 light2"><i class="fa fa-paper-plane"></i>&nbsp; Read more</a>
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fourth">
    
        <div class="flips3">
        
            <div class="flips3_front flipscont3">
            	<i class="fa fa-thumbs-o-up"></i>
            	<h5>Branding</h5>
            </div>
            
            <div class="flips3_back flipscont3">
            	<h5 class="white">Branding</h5>
                <p>Lorem Ipsum as the default will uncover many web sites versions have years.</p>
                <br />
                <a href="#" class="but_small5 light2"><i class="fa fa-paper-plane"></i>&nbsp; Read more</a>
            </div>
            
        </div>
    
    </div><!-- end section -->
    
    <div class="one_fourth last">
    
        <div class="flips3">
        
            <div class="flips3_front flipscont3">
            	<i class="fa fa-envelope-o"></i>
            	<h5>Presentation</h5>
            </div>
            
            <div class="flips3_back flipscont3">
            	<h5 class="white">Presentation</h5>
                <p>Lorem Ipsum as the default will uncover many web sites versions have years.</p>
                <br />
                <a href="#" class="but_small5 light2"><i class="fa fa-paper-plane"></i>&nbsp; Read more</a>
            </div>
            
        </div>
    
    </div><!-- end section -->
    
</div><!-- end all sections -->

<div class="clearfix"></div>

<div class="features_sec49 two" style="background: #f4f4f4;">
<div class="container">

	<h2>Need help? Ready to Help you with Whatever you Need</h2>
	
    <strong>+255 754 025098</strong> <em>Answer Desk is Ready!</em>
    
</div>
</div><!-- end features section 49 -->

<div id="owl-demo" class="owl-carousel">

	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	
	

</div><!-- end section -->

@endsection