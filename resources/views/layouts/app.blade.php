<!doctype html>
<html lang="en-gb" class="no-js"> <!--<![endif]-->



@include('layouts.header')

<body>

<div class="site_wrapper">

@include('layouts.headerinfo')

<div class="clearfix"></div>

@include('layouts.menu')

<div class="clearfix"></div>

<!-- Slider
======================================= -->  

@yield('content')




@include('layouts.footer')


</body>


</html>
