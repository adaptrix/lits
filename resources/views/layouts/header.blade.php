<head>
	<title>Legendary IT Solutions</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    <link rel="shortcut icon" type="image/png" href="{{asset('images/favicon.png')}}"/>
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="{{asset('images/favicon.html')}}">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link rel="stylesheet" href="{{asset('css/reset.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" />
    
    <!-- font awesome icons -->
    <link rel="stylesheet" href="{{asset('css/font-awesome/css/font-awesome.min.css')}}">
	
    <!-- simple line icons -->
	<link rel="stylesheet" type="text/css" href="{{asset('css/simpleline-icons/simple-line-icons.css')}}" media="screen" />
    
    <!-- animations -->
    <link href="{{asset('js/animations/css/animations.min.css')}}" rel="stylesheet" type="text/css" media="all" />
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="{{asset('css/responsive-leyouts.css')}}" type="text/css" />
    
    <!-- shortcodes -->
    <link rel="stylesheet" media="screen" href="{{asset('css/shortcodes.css')}}" type="text/css" /> 
    
<!-- just remove the below comments witch color skin you want to use -->
    <link rel="stylesheet" href="{{asset('css/colors/blue.css')}}" />
    <!--<link rel="stylesheet" href="css/colors/green.css" />-->
    <!--<link rel="stylesheet" href="css/colors/cyan.css" />-->
    <!--<link rel="stylesheet" href="css/colors/orange.css" />-->
    <!--<link rel="stylesheet" href="css/colors/lightblue.css" />-->
    <!--<link rel="stylesheet" href="css/colors/pink.css" />-->
    <!--<link rel="stylesheet" href="css/colors/purple.css" />-->
    <!--<link rel="stylesheet" href="css/colors/bridge.css" />-->
    <!--<link rel="stylesheet" href="css/colors/slate.css" />-->
    <!--<link rel="stylesheet" href="css/colors/yellow.css" />-->
    <!--<link rel="stylesheet" href="css/colors/darkred.css" />-->

<!-- just remove the below comments witch bg patterns you want to use --> 
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-default.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-one.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-two.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-three.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-four.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-five.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-six.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-seven.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-eight.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-nine.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-ten.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-eleven.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-twelve.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-thirteen.css" />-->
    
    <!-- style switcher -->
    <link rel = "stylesheet" media = "screen" href = "{{asset('js/style-switcher/color-switcher.css')}}" />
    
    <!-- mega menu -->
    <link href="{{asset('js/mainmenu/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('js/mainmenu/menu.css')}}" rel="stylesheet">
    
    <!-- slide panel -->
    <link rel="stylesheet" type="text/css" href="{{asset('js/slidepanel/slidepanel.css')}}">
    
    <!-- MasterSlider -->
	<link rel="stylesheet" href="{{asset('js/masterslider/style/masterslider.css')}}" />
    <link rel="stylesheet" href="{{asset('js/masterslider/skins/default/style.css')}}" />
	
    <!-- owl carousel -->
    <link href="{{asset('js/carouselowl/owl.transitions.css')}}" rel="stylesheet">
    <link href="{{asset('js/carouselowl/owl.carousel.css')}}" rel="stylesheet">
	<link href="{{asset('js/carouselowl/owl.theme.html')}}" rel="stylesheet">
    
    <!-- icon hover -->
    <link rel="stylesheet" href="{{asset('js/iconhoverefs/component.css')}}" />
	
	
     
    
  	<!-- basic slider -->
    <link rel="stylesheet" href="{{asset('js/basicslider/bacslider.css')}}" />
    
    <!-- cubeportfolio -->
    <link rel="stylesheet" type="text/css" href="{{asset('js/cubeportfolio/cubeportfolio.min.css')}}">
    
    <!-- flexslider -->
    <link rel="stylesheet" href="{{asset('js/flexslider/flexslider.css')}}" type="text/css" media="screen" />
 	<link rel="stylesheet" type="text/css" href="{{asset('js/flexslider/skin.css')}}" />
    
    <!-- tabs -->
    <link rel="stylesheet" type="text/css" href="{{asset('js/tabs/assets/css/responsive-tabs3.css')}}">
    
    <!-- accordion -->
    <link rel="stylesheet" type="text/css" href="{{asset('js/accordion/style.css')}}" />
	
	<!-- progressbar -->
  	<link rel="stylesheet" href="{{asset('js/progressbar/ui.progress-bar.css')}}">
    
    <!-- classyloader-->
    <script type="text/javascript" src="{{asset('js/universal/jquery.js')}}"></script>
    <script src="{{asset('js/classyloader/jquery.classyloader.min.js')}}"></script>
	
	<!-- timeline -->
  	<link rel="stylesheet" href="{{asset('js/timeline/timeline.css')}}">
    
    <!-- image hover effects -->
  	<link rel="stylesheet" href="{{asset('js/ihovereffects/main.css')}}">
	
	<!-- forms -->
    <link rel="stylesheet" href="{{asset('js/form/sky-forms2.css')}}" type="text/css" media="all">
	
	
    
    
    
</head>