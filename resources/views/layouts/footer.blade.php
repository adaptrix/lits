<footer class="footer">


<div class="clearfix"></div>

<div class="copyright_info">
<div class="container">

	
    
    <div class="one_half animate" data-anim-type="fadeInRight" data-anim-delay="300">
    
        Copyright &copy; 2018 Legendary IT Solutions. All rights reserved.  <a href="#" style="color: #3183d7;">Terms & Conditions</a></a>
        
    </div>
    
    <div class="one_half last" style="margin-right: 0px !important;">
        
        <ul class="footer_social_links">
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="300">
				<a href="https://www.facebook.com/LITS786" target="_blank">
					<i class="fa fa-facebook" style="background: #3B5998;color: white;"></i>
				</a>
			</li>
			<li class="animate" data-anim-type="zoomIn" data-anim-delay="300">
				<a href="https://www.instagram.com/legendary_it_solutions/" target="_blank">
					<i class="fa fa-instagram" style="background: #125688;color: white;"></i>
				</a>
			</li>
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="300">
				<a href="https://plus.google.com/+Legendaryits/videos" target="_blank">
					<i class="fa fa-google-plus" style="background: #E1CACA;color: white;"></i>
				</a>			
			</li>
			
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="300">
				<a href="https://www.linkedin.com/company/legendary-it-solution?trk=nav_account_sub_nav_company_admin" target="_blank">
					<i class="fa fa-linkedin" style="background: #007bb5;color: white;"></i>
				</a>
			</li>
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="300">
				<a href="https://www.youtube.com/channel/UC-52AaUZml1TyT5tz1QsoXw" target="_blank">
					<i class="fa fa-youtube" style="background: #bb0000;color: white;"></i>
				</a>
			</li>
        </ul>
            
    </div>
    
</div>
</div><!-- end copyright info -->


</footer>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->


<!-- style switcher -->
<script type="text/javascript" src="{{asset('js/style-switcher/styleswitcher.js')}}"></script>
<link rel="alternate stylesheet" type="text/css" href="{{asset('css/colors/blue.css')}}" title="blue" />
<!--<link rel="alternate stylesheet" type="text/css" href="{{asset('css/colors/green.css')}}" title="green" />
<link rel="alternate stylesheet" type="text/css" href="{{asset('css/colors/cyan.css')}}" title="cyan" />
<link rel="alternate stylesheet" type="text/css" href="{{asset('css/colors/orange.css')}}" title="orange" />
<link rel="alternate stylesheet" type="text/css" href="{{asset('css/colors/lightblue.css')}}" title="lightblue" />
<link rel="alternate stylesheet" type="text/css" href="{{asset('css/colors/pink.css')}}" title="pink" />
<link rel="alternate stylesheet" type="text/css" href="{{asset('css/colors/purple.css')}}" title="purple" />
<link rel="alternate stylesheet" type="text/css" href="{{asset('css/colors/bridge.css')}}" title="bridge" />
<link rel="alternate stylesheet" type="text/css" href="{{asset('css/colors/slate.css')}}" title="slate" />
<link rel="alternate stylesheet" type="text/css" href="{{asset('css/colors/yellow.css')}}" title="yellow" />
<link rel="alternate stylesheet" type="text/css" href="{{asset('css/colors/darkred.css')}}" title="darkred" />-->




</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="{{asset('js/universal/jquery.js')}}"></script>

<!-- style switcher -->
<script src="{{asset('js/style-switcher/jquery-1.js')}}"></script>
<script src="{{asset('js/style-switcher/styleselector.js')}}"></script>





<!--<script type="text/javascript">
(function($) {
 "use strict";

	var slider = new MasterSlider();
	// adds Arrows navigation control to the slider.
	slider.control('arrows');
	slider.control('bullets');

	 slider.setup('masterslider' , {
		 width:1400,    // slider standard width
		 height:750,   // slider standard height
		 space:0,
		 speed:45,
		 layout:'fullwidth',
		 loop:true,
		 preload:0,
		 autoplay:true,
		 view:"fade"
	});
	
	var slider2 = new MasterSlider();

	 slider2.setup('masterslider2' , {
		 width:1400,    // slider standard width
		 height:520,   // slider standard height
		 space:0,
		 speed:45,
		 layout:'fullwidth',
		 loop:true,
		 preload:0,
		 autoplay:true,
		 view:"basic"
	});
	
})(jQuery);
</script>-->






<!-- flexslider -->
<script defer src="{{asset('js/flexslider/jquery.flexslider.js')}}"></script>
<script defer src="{{asset('js/flexslider/custom.js')}}"></script>



<!-- basic slider -->
<script type="text/javascript" src="{{asset('js/basicslider/bacslider.js')}}"></script>
<script type="text/javascript">
(function($) {
 "use strict";
 
	$(document).ready(function() {
		$(".main-slider-container").sliderbac();
	});
	
})(jQuery);
</script>

<!-- tabs -->
<script src="{{asset('js/tabs/assets/js/responsive-tabs.min.js')}}" type="text/javascript"></script>

<!-- animations -->
<script src="{{asset('js/animations/js/animations.min.js')}}" type="text/javascript"></script>

<!-- slide panel -->
<script type="text/javascript" src="{{asset('js/slidepanel/slidepanel.js')}}"></script>

<!-- mega menu -->
<script src="{{asset('js/mainmenu/bootstrap.min.js')}}"></script> 
<script src="{{asset('js/mainmenu/customeUI.js')}}"></script> 

<!-- MasterSlider -->
<script src="{{asset('js/masterslider/jquery.easing.min.js')}}"></script>
<script src="{{asset('js/masterslider/masterslider.min.js')}}"></script>

<!-- scroll up -->
<script src="{{asset('js/scrolltotop/totop.js')}}" type="text/javascript"></script>

<!-- sticky menu -->
<script type="text/javascript" src="{{asset('js/mainmenu/sticky-main.js')}}"></script>
<script type="text/javascript" src="{{asset('js/mainmenu/modernizr.custom.75180.js')}}"></script>

<!-- cubeportfolio -->
<script type="text/javascript" src="{{asset('js/cubeportfolio/jquery.cubeportfolio.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/cubeportfolio/main.js')}}"></script>
<script type="text/javascript" src="{{asset('js/cubeportfolio/main2.js')}}"></script>

<!-- Accordion-->
<script type="text/javascript" src="{{asset('js/accordion/jquery.accordion.js')}}"></script>
<script type="text/javascript" src="{{asset('js/accordion/custom.js')}}"></script>

<!-- progress bar -->
<script src="{{asset('js/progressbar/progress.js')}}" type="text/javascript" charset="utf-8"></script>

<!-- owl carousel -->
<script src="{{asset('js/carouselowl/owl.carousel.js')}}"></script>
<script src="{{asset('js/carouselowl/custom.js')}}"></script>

<script src="{{asset('js/timeline/modernizr.js')}}"></script>
<script>
(function($) {
 "use strict";

jQuery(document).ready(function($){
	var $timeline_block = $('.cd-timeline-block');

	//hide timeline blocks which are outside the viewport
	$timeline_block.each(function(){
		if($(this).offset().top > $(window).scrollTop()+$(window).height()*0.75) {
			$(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
		}
	});

	//on scolling, show/animate timeline blocks when enter the viewport
	$(window).on('scroll', function(){
		$timeline_block.each(function(){
			if( $(this).offset().top <= $(window).scrollTop()+$(window).height()*0.75 && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) {
				$(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
			}
		});
	});
});

})(jQuery);
</script>
<script type="text/javascript" src="{{asset('js/universal/custom.js')}}"></script>

