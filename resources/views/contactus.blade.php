@extends('layouts.app')

@section('content')

<div class="page_title">
<div class="container">

    <div class="title"><h1>Contact Us</h1></div>
        
	<h3>Storytelling fused with technology and design.</h3>
	<h5>Anchour enhances business with its proven web media services.</h5>
    
</div>
</div><!-- end page title -->

<div class="clearfix"></div>

<div class="content_fullwidth less">

<div class="one_full">
	
	
	<iframe class="google-map2" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15846.498109124039!2d39.283936!3d-6.81544!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x997b8af2a605797!2sLegendary+IT+Solutions!5e0!3m2!1sen!2stz!4v1544179073055"   style="border:0" allowfullscreen></iframe>
</div><!-- end google map -->

<div class="clearfix marb10"></div>

<div class="container">

      <div class="two_third">
      
       
        
        <div class="cforms">
        
        <form action="http://gsrthemes.com/aaika/fullwidth/demo-contacts.php" method="post" id="sky-form" class="sky-form">
          <header>Send Us a <strong>Message!</strong></header>
          <fieldset>
            <div class="row">
              <section class="col col-6">
                <label class="label">Name</label>
                <label class="input"> <i class="icon-append icon-user"></i>
                  <input type="text" name="name" id="name">
                </label>
              </section>
              <section class="col col-6">
                <label class="label">E-mail</label>
                <label class="input"> <i class="icon-append icon-envelope-alt"></i>
                  <input type="email" name="email" id="email">
                </label>
              </section>
            </div>
			
            <section>
              <label class="label">Phone Number</label>
              <label class="input"> <i class="icon-append icon-tag"></i>
                <input type="text" name="subject" id="subject">
              </label>
            </section>
            <section>
              <label class="label">Message</label>
              <label class="textarea"> <i class="icon-append icon-comment"></i>
                <textarea rows="4" name="message" id="message"></textarea>
              </label>
            </section>
          </fieldset>
          <footer>
            <button type="submit" class="button">Send message</button>
          </footer>
          <div class="message"> <i class="icon-ok"></i>
            <p>Your message was successfully sent!</p>
          </div>
        </form>
        
        </div>
        
      </div><!-- end section -->
      
      <div class="one_third last">
      
        <div class="address_info two">
        
          <h4 class="light">Address</h4>
          <ul>
            <li> <strong>Legendary IT Solutions</strong><br />
              Mtendeni Street, <br />
			  P.O.Box 2632 Dar es Salaam, Tanzania<br />
              Phone #: +255 754 025098 / +255 754 025098<br />
              E-mail: <a href="mailto:info@legendaryits.com">info@legendaryits.com</a><br />
              Website: <a href="http://legendaryits.com" target='_blank'>Legendary IT Solutions</a> </li>
          </ul>
          
        </div>
        
	  </div>
        

</div>
</div><!-- end content area -->

@endsection