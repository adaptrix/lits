@extends('layouts.app')

@section('content')

<div class="mstslider">

<!-- masterslider -->
<div class="master-slider ms-skin-default" id="masterslider">

    <!-- slide -->
   	<div class="ms-slide slide-1" data-delay="7">
         
        <!-- slide background -->
        <img src="masterslider/blank.html" data-src="images/sliders/master/slider-bg.png" alt=""/>     
        
         <img src="../masterslider/blank.html" data-src="images/sliders/master/slide1.png" alt="" class="ms-layer"
            style="bottom:0; right:30px"
            data-effect="right(72)"
            data-delay="0"
            data-duration="300"
            data-type="image"
		/>
        
        <div class="ms-layer text1"
        	style="left:100px; top:285px"
            data-effect="bottom(50)"
            data-duration="900"
            data-delay="300"
            data-ease="easeOutExpo"
        >Everything you need <strong>Build an Awesome<br />Website!</strong></div>
        
        <div class="ms-layer text2"
        	style="left:120px; top:465px"
            data-effect="left(100)"
            data-duration="2000"
            data-delay="500"
            data-ease="easeOutExpo"
        >Many web sites still their infancy various versions have<br />packages sure there anything over the years.</div>
         
         <div class="ms-layer text3"
        	style="left:125px; top:560px"
            data-effect="left(100)"
            data-duration="2000"
            data-delay="900"
            data-ease="easeOutExpo"
        >Clean, Professional<br />Modern Design</div>
        
        <div class="ms-layer text4"
        	style="left:250px; top:554px"
            data-effect="right(100)"
            data-duration="2000"
            data-delay="900"
            data-ease="easeOutExpo"
        >Done For You</div>
        
        <div class="ms-layer graph1"
        	style="right:405px; top:184px"
            data-effect="bottom(50)"
            data-duration="2000"
            data-delay="1200"
            data-ease="easeOutExpo"
        >Successful <strong>Business</strong></div>
        
        <div class="ms-layer graph2"
        	style="right:605px; top:255px"
            data-effect="bottom(50)"
            data-duration="2000"
            data-delay="1400"
            data-ease="easeOutExpo"
        >Values</div>
        
		<div class="ms-layer graph2"
        	style="right:200px; top:255px"
            data-effect="bottom(50)"
            data-duration="2000"
            data-delay="1600"
            data-ease="easeOutExpo"
        >Vision</div>
        
        <div class="ms-layer graph3"
        	style="right:705px; top:355px"
            data-effect="bottom(50)"
            data-duration="2000"
            data-delay="1700"
            data-ease="easeOutExpo"
        >Strategy</div>
        
        <div class="ms-layer graph3"
        	style="right:320px; top:265px"
            data-effect="bottom(50)"
            data-duration="2000"
            data-delay="1800"
            data-ease="easeOutExpo"
        >Teamwork</div>
        
        <div class="ms-layer graph4"
        	style="right:520px; top:340px"
            data-effect="bottom(30)"
            data-duration="2000"
            data-delay="1900"
            data-ease="easeOutExpo"
        >Plan</div>
        
        <div class="ms-layer graph4"
        	style="right:307px; top:350px"
            data-effect="bottom(30)"
            data-duration="2000"
            data-delay="2000"
            data-ease="easeOutExpo"
        >Mision</div>
        
	</div><!-- end slide -->
	
	<!-- slide -->
   	<div class="ms-slide slide-2" data-delay="7">
	
         
        <!-- slide background -->
        <img src="masterslider/blank.html" data-src="images/sliders/master/slider-bg.png" alt=""/>     
        
         <img src="../masterslider/blank.html" data-src="images/sliders/master/slide2.png" alt="" class="ms-layer"
            style="bottom:0; right:70px"
            data-effect="right(72)"
            data-delay="0"
            data-duration="300"
            data-type="image"
		/>
        
        <div class="ms-layer text1"
        	style="left:100px; top:275px"
            data-effect="bottom(50)"
            data-duration="900"
            data-delay="300"
            data-ease="easeOutExpo"
        >Looks Great on <strong>Tablets and Mobile <br /> Devices</strong></div>
        
        <div class="ms-layer text2"
        	style="left:120px; top:455px"
            data-effect="left(100)"
            data-duration="2000"
            data-delay="500"
            data-ease="easeOutExpo"
        >Many web sites still their infancy various versions have<br />packages sure there anything over the years.</div>
         
         <div class="ms-layer text3"
        	style="left:125px; top:550px"
            data-effect="left(100)"
            data-duration="2000"
            data-delay="900"
            data-ease="easeOutExpo"
        >Clean, Professional<br />Modern Design</div>
        
        <div class="ms-layer text4"
        	style="left:250px; top:544px"
            data-effect="right(100)"
            data-duration="2000"
            data-delay="900"
            data-ease="easeOutExpo"
        >Done For You</div>
         
        
	</div><!-- end slide -->
    
    
    <!-- slide -->
   	<div class="ms-slide slide-3" data-delay="7">
         
        <!-- slide background -->
        <img src="masterslider/blank.html" data-src="images/sliders/master/slide3.png" alt=""/>     

		<div class="ms-layer centext text1"
        	style="top:250px"
            data-effect="bottom(50)"
            data-duration="900"
            data-delay="300"
            data-ease="easeOutExpo"
        ><strong>Pro Themes Hope you Enjoy</strong></div>
        
        <div class="ms-layer centext text2"
        	style="top:325px"
            data-effect="bottom(50)"
            data-duration="2000"
            data-delay="500"
            data-ease="easeOutExpo"
        >Many web sites still their infancy various versions have over the years.</div>
         
         <div class="ms-layer text3"
        	style="left:475px; top:392px"
            data-effect="left(100)"
            data-duration="2000"
            data-delay="900"
            data-ease="easeOutExpo"
        >Clean, Professional<br />Modern Design</div>
        
        <div class="ms-layer text4"
        	style="left:605px; top:385px"
            data-effect="right(100)"
            data-duration="2000"
            data-delay="900"
            data-ease="easeOutExpo"
        >Done For You</div>
        
        
	</div><!-- end slide -->

</div><!-- end of masterslider -->

</div><!-- end slider -->

<div class="margin_top1"></div>
<div class="clearfix"></div>
        
<div id="owl-demo" class="owl-carousel">

	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	<div class="item"><img src="images/media/ulfa.png" alt="" style="width: auto;"/></div>
	
	

</div><!-- end section -->


<div class="margin_top1"></div>
<div class="clearfix"></div>


<div class="features_sec7">
<div class="container">

	<div class="features_sec45" style="padding:0px;">
<div class="container">
	
    <h6>- Welcome to Legendary IT Solutions -</h6>
    
    <h1>Professional <em style="color:#3183d7;">Creativity with Clean Theme</em>
    <b>Let's Start an Awesome Project Now!</b></h1>
    
	
    
</div>
</div>
	
    <div class="clearfix margin_top2"></div>
    
    <div class="one_fourth animate" data-anim-type="fadeInUp" data-anim-delay="300">
    	
        <div class="box">
        
        	<div class="ibox"><img src="images/site-img20.png" alt="" /> <h6>Business Ideas</h6></div>
        	
            <p>Lorem Ipsum which even looks reasonable and the generated therefore here always.</p>

            <a href="#" class="tbut">Read More</a>
            
        </div>
        
	</div><!-- end section -->
    
    <div class="one_fourth animate" data-anim-type="fadeInUp" data-anim-delay="350">
    	
        <div class="box">
        
        	<div class="ibox"><img src="images/site-img21.png" alt="" /> <h6>Small Business</h6></div>
        	
            <p>Lorem Ipsum which even looks reasonable and the generated therefore here always.</p>

            <a href="#" class="tbut">Read More</a>
            
        </div>
        
	</div><!-- end section -->
    
    <div class="one_fourth animate" data-anim-type="fadeInUp" data-anim-delay="400">
    	
        <div class="box">
        
        	<div class="ibox"><img src="images/site-img22.png" alt="" /> <h6>Our Comments</h6></div>
        	
            <p>Lorem Ipsum which even looks reasonable and the generated therefore here always.</p>

            <a href="#" class="tbut">Read More</a>
            
        </div>
        
	</div><!-- end section -->
    
    <div class="one_fourth last animate" data-anim-type="fadeInUp" data-anim-delay="450">
    	
        <div class="box">
        
        	<div class="ibox"><img src="images/site-img23.png" alt="" /> <h6>Open 24 hours</h6></div>
        	
            <p>Lorem Ipsum which even looks reasonable and the generated therefore here always.</p>

            <a href="#" class="tbut">Read More</a>
            
        </div>
        
	</div><!-- end section -->

</div>
</div><!-- end features section 7 -->


<div class="clearfix"></div>

<div class="recent_works">

	
    <div class="title2">
    	<h2><span class="line"></span><span class="text">Recent Works</span><em>Aipsum therefore always</em></h2>
    </div>
    
    <div class="margin_top2"></div>
    <div class="clearfix"></div>
    
    <div id="grid-container" class="cbp-l-grid-fullScreen animate" data-anim-type="fadeInUp" data-anim-delay="100">
    
        <ul>
        
            <li class="cbp-item">
                <a href="images/media/portfolio-img1.jpg" class="cbp-caption cbp-lightbox" data-title="Dashboard<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img1.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Dashboard</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            
            
            <li class="cbp-item">
                <a href="images/media/portfolio-img2.jpg" class="cbp-caption cbp-lightbox" data-title="Client chat app WIP<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img2.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Client chat app WIP</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            
            
            <li class="cbp-item">
                <a href="https://www.youtube.com/watch?v=dChhzNGHgnA" class="cbp-caption cbp-lightbox" data-title="World Clock Widget<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img3.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">World Clock Widget</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            
            
            <li class="cbp-item">
                <a href="images/media/portfolio-img4.jpg" class="cbp-caption cbp-lightbox" data-title="Website Lightbox<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img4.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Website Lightbox</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            
            
            <li class="cbp-item">
                <a href="http://vimeo.com/156783#" class="cbp-caption cbp-lightbox" data-title="Skateshop Website<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img5.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Skateshop Website</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            
            
            <li class="cbp-item">
                <a href="images/media/portfolio-img6.jpg" class="cbp-caption cbp-lightbox" data-title="10 Navigation Bars<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img6.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">10 Navigation Bars</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            
            
            <li class="cbp-item">
                <a href="images/media/portfolio-img7.jpg" class="cbp-caption cbp-lightbox" data-title="To-Do Dashboard<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img7.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">To-Do Dashboard</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            
            
            <li class="cbp-item">
                <a href="images/media/portfolio-img8.jpg" class="cbp-caption cbp-lightbox" data-title="Events and  More<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img8.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Events and More</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li>


        </ul>
        
    </div>
    
	<div class="cbp-l-loadMore-text">
        <div data-href="ajax/loadMore.html" class="cbp-l-loadMore-text-link">
			<a href="#" class="but_small2">View all</a>
		</div>
    </div>

</div><!-- end recent works -->

<div class="clearfix"></div>







<div class="parallax_section1">
<div class="container">
	
    

</div>
</div><!-- end parallax section 1 -->

<div class="clearfix"></div>

<div class="features_sec4">
<div class="container">
	
    <div class="onecol_sixty">
    
    	<h3 class="unline"><i class="fa fa-comments"></i> Latest News & Events</h3>
    	
        <div class="clearfix"></div>
                
		<div class="lblogs animate" data-anim-type="fadeIn" data-anim-delay="300">
        
			<div class="lbimg"><img src="images/site-img7.jpg" alt="" /> <span><strong>DEC</strong> 14</span> </div>
        	
            <h5>Have evolved many web sites</h5>
            
            <a href="#" class="smlinks"><i class="fa fa-eye"></i> 45</a>
            <a href="#" class="smlinks"><i class="fa fa-comment"></i> 18</a>
            <a href="#" class="smlinks"><i class="fa fa-heart"></i> 89</a>
            
            <p>Lorem Ipsum which looks reasonable the generated Ipsum therefore always.</p>
            
            <a href="#" class="remobut">Read More</a>
            
        </div><!-- end section -->
    	
        <div class="lblogs last animate" data-anim-type="fadeIn" data-anim-delay="400">
        
			<div class="lbimg"><img src="images/site-img8.jpg" alt="" /> <span><strong>DEC</strong> 13</span> </div>
        	
            <h5>Desktop publishing packages</h5>
            
            <a href="#" class="smlinks"><i class="fa fa-eye"></i> 25</a>
            <a href="#" class="smlinks"><i class="fa fa-comment"></i> 3</a>
            <a href="#" class="smlinks"><i class="fa fa-heart"></i> 10</a>
            
            <p>Lorem Ipsum which looks reasonable the generated Ipsum therefore always.</p>
            
            <a href="#" class="remobut">Read More</a>
            
        </div><!-- end section -->
        
    </div><!-- end all sections -->
    
	<div class="onecol_forty last">
    
		<div class="peosays">
        
            <h3 class="unline"><i class="fa fa-users"></i> What People Says</h3>
            
            <div class="clearfix"></div>
            
            <div id="owl-demo11" class="owl-carousel small four">
                
            	<div class="box">
                
                	<div class="ppimg"><img src="images/site-img11.jpg" alt="" /> <h6>Riwan Jaffer</h6></div>
                    
                    <p>Legendary helped me make my website and what an awesome job they did! Kudos to Sarfaraz for being so patient with me and my little little changes! And just when he was done i again requested changes while holding my breath, to my surprise he was as cool as it can be! I would definetely recommend Legendary IT solutions to anyone</p>
                    
                    <span> Rating: &nbsp; <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span>
                    
                </div><!-- end slide -->
                
                <div class="box">
                
                	<div class="ppimg"><img src="images/site-img11.jpg" alt="" /> <h6>Mohammed M. Remtulla  </h6></div>
                    
                    <p>Its been more than a year I've been working with you and I've received excellent customer service and I love how you designed the company Website.</p>
                    
                    <span> Rating: &nbsp; <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span>
                    
                </div><!-- end slide -->
                
                <div class="box">
                
                	<div class="ppimg"><img src="images/site-img11.jpg" alt="" /> <h6>Azhar Rajan </h6></div>
                    
                    <p>Got my website designed by them and it is just fantastic. done on time. perfect design approved the first time and excellent customer support wherever i failed and needed them even after the website was done.</p>
                    
                    <span> Rating: &nbsp; <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span>
                    
                </div><!-- end slide -->
                
               
                    
            </div>
        
        </div>

     </div>
    
</div>
</div><!-- end features section 4 -->

<div class="clearfix"></div>



<div class="clearfix margin_top9"></div>

<div class="features_sec13">
	
    <img src="images/fullsite-img5.jpg" alt="" class="animate" data-anim-type="fadeIn" data-anim-delay="300" />
    
</div><!-- end features section 13 -->

<div class="clearfix"></div>

@endsection