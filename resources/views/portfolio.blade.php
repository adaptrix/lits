@extends('layouts.app')

@section('content')

<div class="page_title">
<div class="container">

    <div class="title"><h1>Portfolio</h1></div>
        
	<h3>Storytelling fused with technology and design.</h3>
	<h5>Anchour enhances business with its proven web media services.</h5>
    
</div>
</div><!-- end page title -->

<div class="clearfix"></div>
<div class="content_fullwidth">
<div class="recent_works2">
	<div class="container">

    <div id="filters-container" class="cbp-l-filters-alignCenter">
        <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">
            ALL <div class="cbp-filter-counter"></div>
        </div> /
        <div data-filter=".identity" class="cbp-filter-item">
            IDENTITY <div class="cbp-filter-counter"></div>
        </div> /
        <div data-filter=".web-design" class="cbp-filter-item">
            WEB DESIGN <div class="cbp-filter-counter"></div>
        </div> /
        <div data-filter=".graphic" class="cbp-filter-item">
            GRAPHIC <div class="cbp-filter-counter"></div>
        </div> /
        <div data-filter=".logo" class="cbp-filter-item">
            LOGO <div class="cbp-filter-counter"></div>
        </div> /
        <div data-filter=".motion" class="cbp-filter-item">
            MOTION <div class="cbp-filter-counter"></div>
        </div>
    </div>
    
    <div id="grid-container" class="cbp-l-grid-fullScreen three">
    
        <ul>
        
            <li class="cbp-item identity">
                <a href="images/media/portfolio-img1.jpg" class="cbp-caption cbp-lightbox" data-title="Dashboard<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img1.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Dashboard</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
            
            
            <li class="cbp-item identity">
                <a href="images/media/portfolio-img2.jpg" class="cbp-caption cbp-lightbox" data-title="Client chat app WIP<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img2.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Client chat app WIP</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
            
            
            <li class="cbp-item web-design motion">
                <a href="images/media/portfolio-img3.jpg" class="cbp-caption cbp-lightbox" data-title="World Clock Widget<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img3.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">World Clock Widget</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
            
            
            <li class="cbp-item web-design">
                <a href="images/media/portfolio-img4.jpg" class="cbp-caption cbp-lightbox" data-title="Website Lightbox<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img4.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Website Lightbox</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
            
            
            <li class="cbp-item web-design">
                <a href="images/media/portfolio-img5.jpg" class="cbp-caption cbp-lightbox" data-title="Skateshop Website<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img5.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Skateshop Website</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
            
            
            <li class="cbp-item graphic motion">
                <a href="images/media/portfolio-img6.jpg" class="cbp-caption cbp-lightbox" data-title="10 Navigation Bars<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img6.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">10 Navigation Bars</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
            
            
            <li class="cbp-item graphic">
                <a href="images/media/portfolio-img7.jpg" class="cbp-caption cbp-lightbox" data-title="To-Do Dashboard<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img7.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">To-Do Dashboard</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
            
            
            <li class="cbp-item logo motion">
                <a href="images/media/portfolio-img8.jpg" class="cbp-caption cbp-lightbox" data-title="Events and  More<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img8.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Events and More</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
			
            <li class="cbp-item identity">
                <a href="images/media/portfolio-img1.jpg" class="cbp-caption cbp-lightbox" data-title="Dashboard<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img1.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Dashboard</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
            
            
            <li class="cbp-item identity">
                <a href="images/media/portfolio-img2.jpg" class="cbp-caption cbp-lightbox" data-title="Client chat app WIP<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img2.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Client chat app WIP</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
            
            
            <li class="cbp-item web-design">
                <a href="images/media/portfolio-img3.jpg" class="cbp-caption cbp-lightbox" data-title="World Clock Widget<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img3.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">World Clock Widget</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
            
            
            <li class="cbp-item web-design motion">
                <a href="images/media/portfolio-img4.jpg" class="cbp-caption cbp-lightbox" data-title="Website Lightbox<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img4.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Website Lightbox</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
            
            
            <li class="cbp-item web-design">
                <a href="images/media/portfolio-img5.jpg" class="cbp-caption cbp-lightbox" data-title="Skateshop Website<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img5.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Skateshop Website</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
            
            
            <li class="cbp-item graphic">
                <a href="images/media/portfolio-img6.jpg" class="cbp-caption cbp-lightbox" data-title="10 Navigation Bars<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img6.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">10 Navigation Bars</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
            
            
            <li class="cbp-item graphic motion">
                <a href="images/media/portfolio-img7.jpg" class="cbp-caption cbp-lightbox" data-title="To-Do Dashboard<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img7.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">To-Do Dashboard</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
            
            
            <li class="cbp-item logo">
                <a href="images/media/portfolio-img8.jpg" class="cbp-caption cbp-lightbox" data-title="Events and  More<br>by GSRthemes9">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/media/portfolio-img8.jpg" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title">Events and More</div>
                                <div class="cbp-l-caption-desc">by GSRthemes9</div>
                            </div>
                        </div>
                    </div>
                </a>
            </li><!-- end item -->
            
        </ul>
        
    </div>
    
	<div class="cbp-l-loadMore-text">
        <div data-href="ajax/loadMore.html" class="cbp-l-loadMore-text-link"></div>
    </div>
    
	</div>
</div>
</div><!-- end content area -->

@endsection